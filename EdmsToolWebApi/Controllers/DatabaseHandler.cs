﻿using EdmsToolWebApi.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Controllers
{
    public class DatabaseHandler
    {

        static String DATABASE_PATH = AppDomain.CurrentDomain.GetData("DataDirectory").ToString() + "\\edms_api_db.sqlite";// "~/App_Data/edms_api_db.sqlite";
      
        public static SQLiteConnection dbConnection()
        {

            SQLiteConnection sqlConnection;

            if (!(new DatabaseHandler().databaseExists(DATABASE_PATH)))
            {

            }

            sqlConnection = new SQLiteConnection(DATABASE_PATH, true);

            if (isApplicationFirstRun())
            {
                //create the tables
                createTheTables(sqlConnection);
            }

            return sqlConnection;

        }

        private static bool isApplicationFirstRun()
        {
            return true;
        }

        private bool databaseExists(string databasePath)
        {
            return File.Exists(databasePath);
        }

        private static void createTheTables(SQLiteConnection sqlConnection)
        {
            sqlConnection.CreateTable<PettyCashVoucherNumber>(); //old table
            sqlConnection.CreateTable<PettyCashVoucherNumberHeadQuarter>();
            sqlConnection.CreateTable<PettyCashVoucherNumberMbale>();
            sqlConnection.CreateTable<PettyCashVoucherNumberMbarara>();
            sqlConnection.CreateTable<PettyCashVoucherNumberGulu>();
            sqlConnection.CreateTable<BtNumber>();
        }

        internal string generateNexPettyCashVoucherNumber(SQLiteConnection dbConn, string regionalOffice)
        {
            string generatedVoucherNo = "";

            if(regionalOffice == Globals.REGIONAL_OFFICE_HEAD_OFFICE)
            {
                PettyCashVoucherNumberHeadQuarter voucher = new PettyCashVoucherNumberHeadQuarter();
                int rows = dbConn.Insert(voucher);

                string voucherNo = "HQ-" + voucher.Id;
                voucher.VoucherNumber = voucherNo;
                dbConn.Update(voucher);
                generatedVoucherNo = voucherNo;

            }else if (regionalOffice == Globals.REGIONAL_OFFICE_MBALE)
            {
                PettyCashVoucherNumberMbale voucher = new PettyCashVoucherNumberMbale();
                int rows = dbConn.Insert(voucher);

                string voucherNo = "MBALE-" + voucher.Id;
                voucher.VoucherNumber = voucherNo;
                dbConn.Update(voucher);
                generatedVoucherNo = voucherNo;

            }
            else if (regionalOffice == Globals.REGIONAL_OFFICE_MBARARA)
            {
                PettyCashVoucherNumberMbarara voucher = new PettyCashVoucherNumberMbarara();
                int rows = dbConn.Insert(voucher);

                string voucherNo = "MBARARA-" + voucher.Id;
                voucher.VoucherNumber = voucherNo;
                dbConn.Update(voucher);
                generatedVoucherNo = voucherNo;

            }
            else if (regionalOffice == Globals.REGIONAL_OFFICE_GULU)
            {
                PettyCashVoucherNumberGulu voucher = new PettyCashVoucherNumberGulu();
                int rows = dbConn.Insert(voucher);

                string voucherNo = "GULU-" + voucher.Id;
                voucher.VoucherNumber = voucherNo;
                dbConn.Update(voucher);
                generatedVoucherNo = voucherNo;
            }

            return generatedVoucherNo;

        }

        internal int generateNextBtNumber(SQLiteConnection dbConn)
        {
            BtNumber btNumber = new BtNumber();
            int rows = dbConn.Insert(btNumber);
            int recordID = btNumber.Id;
            return recordID;
        }

        //internal List<UploadedImage> allImages(SQLiteConnection dbConn)
        //{
        //    var images = dbConn.Table<UploadedImage>().ToList();
        //    return images;
        //}

        //internal void updateImageDetails(SQLiteConnection dbConn, UploadedImage uploadedImage)
        //{
        //    dbConn.Update(uploadedImage);
        //}

        //internal BaseResponse deleteImage(SQLiteConnection dbConn, int id)
        //{
        //    BaseResponse resp = new BaseResponse();
        //    try
        //    {
        //        var image = dbConn.Table<UploadedImage>().Where(v => v.Id == id).FirstOrDefault();
        //        string imagePath = image == null ? "" : image.imagePath;

        //        //delete image from db
        //        dbConn.Delete<UploadedImage>(id);

        //        //delete image on file
        //        ImageHandler.deleteImageFromImagesFolder(imagePath);

        //        resp.statusCode = Globals.STATUS_CODE_SUCCESS;
        //        resp.statusDescription = "Image successfully deleted";
        //        return resp;
        //    }
        //    catch (Exception ex)
        //    {
        //        resp.statusCode = Globals.STATUS_CODE_FAILED;
        //        resp.statusDescription = "Error occurred on deletion. [" + ex.Message + "]";
        //        return resp;
        //    }
        //}

    }

}