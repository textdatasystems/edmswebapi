﻿using EdmsToolWebApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Xml;
using EdmsToolWebApi.edms;
using System.Web.Http.Cors;

namespace EdmsToolWebApi.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HomeController : ApiController
    {

        [HttpPost, Route("api/edmstools/login")]
        public BaseResponse Login(LoginRequest loginRequest)
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                if (!loginRequest.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = loginRequest.errorDescription;
                    return resp;
                }

                //request is valid
                edms.srv edmsService = new edms.srv();
                string authenticationTicket = "";

                //authenticate via EDMS API
                var xmlResponse = (XmlElement)edmsService.AuthenticateUser(loginRequest.username, loginRequest.password);

                if (xmlResponse.GetAttribute("success") == "true")
                {
                    authenticationTicket = xmlResponse.GetAttribute("ticket");
                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "AUTHENTICATION SUCCESSFUL";
                    resp.result = authenticationTicket;
                    return resp;
                }
                else
                {
                    string error = "EDMS ERROR - " + xmlResponse.GetAttribute("error");
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = error;
                    return resp;
                }

            }
            catch (Exception exception)
            {
                string error = Globals.generalError(exception.Message);
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = error;
                return resp;
            }

        }


        [HttpPost, Route("api/edmstools/upload-document")]
        public BaseResponse UploadEdmsDocumentToEdmsServer(UploadDocumentRequest request)
        {

            BaseResponse resp = new BaseResponse();
            
            try
            {

                edms.srv edmsService = new edms.srv();

                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                string targetEdmsPath = request.targetEdmsPath;
                string documentContentsBase64String = request.documentContents;

                //get bytes from Base64 string
                byte[] bytes = System.Convert.FromBase64String(documentContentsBase64String);

                XmlNode xmlResponse;
                xmlResponse = edmsService.UploadDocument(authenticationTicket, targetEdmsPath, bytes);

                if (xmlResponse.Attributes["success"].Value == "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "The Document has been successfully uploaded";
                    return resp;
                }
                else
                {
                    string error = xmlResponse.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        [HttpPost, Route("api/edmstools/new-form-five-from-template")]
        public BaseResponse CreateFormFiveUsingTemplate(CreateFormFiveFromTemplateRequest request)
        {

            String createdDocumentID;
            BaseResponse resp = new BaseResponse();
            try
            {

                edms.srv edmsService = new edms.srv();

                //validate the request
                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                //get authentication ticket
                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }


                //creating the document from edms template       
                string IRNewDocumentPath = request.newDocumentPath;
                string TemplatePath = Globals.EMIS_TEMPLATE_FORM_5;
                
                //generate the xml content with the template fields
                XmlDocument dataContainerXml = buildForm5XMLContent(request);

                System.Xml.XmlNode xml_response = edmsService.CreateDocumentUsingTemplate(authenticationTicket, IRNewDocumentPath, TemplatePath, dataContainerXml.OuterXml);
               
                // check response xml if any error occured.
                if (xml_response.Attributes["success"].Value.ToUpperInvariant() == "TRUE")
                {

                    string documentNameWithExtension = IRNewDocumentPath + ".pdf";

                    //get the document details from EDMS
                    BaseResponseEdmsDocument docResponse = findDocumentByPath(edmsService, authenticationTicket, documentNameWithExtension);
                    createdDocumentID = docResponse.statusCode == Globals.STATUS_CODE_SUCCESS ? docResponse.document.documentID : "";
                    resp.createdEDMSDocumentId = createdDocumentID;

                    //handle setting of the property set
                    List<EdmsPropertySetRow> properties = new List<EdmsPropertySetRow>();
                    var row = new EdmsPropertySetRow();
                    row.fieldName = "Source";
                    row.fieldValue = "EMIS";
                    properties.Add(row);

                    //set the property set
                    //generate the property set
                    string xmlPropertySet = "<Propertysets><propertyset Name=\"" + Globals.EMIS_PROPERTY_SET_FORM_5 + "\"><propertyrow ";
                    foreach (var propSet in properties)
                    {
                        string fieldName = propSet.fieldName;
                        string fieldValue = propSet.fieldValue;
                        string propRow = " " + fieldName + "=\"" + fieldValue + "\" ";
                        xmlPropertySet += propRow;
                    }
                    xmlPropertySet += " /></propertyset></Propertysets>";


                    //attach the property set the document
                    XmlNode xmlRespAttachPropSet = edmsService.AddPropertySetRow(authenticationTicket, documentNameWithExtension, xmlPropertySet);
                    if (xmlRespAttachPropSet.Attributes["success"].Value != "true")
                    {
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR ADDING PROPERTY SET - " + xmlRespAttachPropSet.Attributes["error"].Value;
                        return resp;
                    }

                    //successfully attached the property set, now we update the document type
                    XmlNode xmlRespUpdateDocType = edmsService.UpdateDocumentType(authenticationTicket, documentNameWithExtension, Globals.EMIS_FORM_5_DOCTYPE_ID);
                    if (xmlRespUpdateDocType.Attributes["success"].Value != "true")
                    {
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR ADDING DOCUMENT TYPE - " + xmlRespUpdateDocType.Attributes["error"].Value;
                        return resp;
                    }
                    //attach document type end


                    //template create, send request to forward the document to a workflow

                    //if we are in debug mode, dont send to a workflow
                    if(Globals.IN_DEBUG_MODE == false)
                    {
                        SubmitDocumentToWorkflowRequest submitReq = new SubmitDocumentToWorkflowRequest();
                        submitReq.authenticationTicket = authenticationTicket;
                        submitReq.edmsFlowDefId = Globals.EMIS_WORK_FLOW_ID_FORM_5;
                        submitReq.edmsFolderPath = IRNewDocumentPath + ".pdf";

                        var submitResp = SubmitDocumentToWorkflow(submitReq);

                        if (submitResp.statusCode != Globals.STATUS_CODE_SUCCESS)
                        {
                            resp.statusCode = Globals.STATUS_CODE_FAILED;
                            resp.statusDescription = "EDMS ERROR - The Document has been created using template, but error occurred on submitting to workflow [" + submitResp.statusDescription + "]";
                            return resp;
                        }
                    }                   

                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "Document successfully submitted to workflow.";
                    return resp;

                }
                else
                {
                    string error = xml_response.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR CREATING DOCUMENT FROM TEMPLATE - " + error;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }
            
        }


        [HttpPost, Route("api/edmstools/bt-form-audit-merged-from-template")]
        public BaseResponse CreateBtFormAuditMergedUsingTemplate(CreateBtFormAuditMergedFromTemplateRequest request)
        {

            String createdDocumentID;
            BaseResponse resp = new BaseResponse();
            try
            {

                edms.srv edmsService = new edms.srv();

                //validate the request
                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                //get authentication ticket
                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }


                //creating the document from edms template       
                string IRNewDocumentPath = request.newDocumentPath;
                string TemplatePath = Globals.EMIS_TEMPLATE_PATH_AUDIT_CLAIM;

                //generate the xml content with the template fields
                XmlDocument dataContainerXml = buildBTFormMergedTemplateBody(request);

                System.Xml.XmlNode xml_response = edmsService.CreateDocumentUsingTemplate(authenticationTicket, IRNewDocumentPath, TemplatePath, dataContainerXml.OuterXml);

                // check response xml if any error occured.
                if (xml_response.Attributes["success"].Value.ToUpperInvariant() == "TRUE")
                {

                    string documentNameWithExtension = IRNewDocumentPath + ".pdf";

                    //get the document details from EDMS
                    BaseResponseEdmsDocument docResponse = findDocumentByPath(edmsService, authenticationTicket, documentNameWithExtension);
                    createdDocumentID = docResponse.statusCode == Globals.STATUS_CODE_SUCCESS ? docResponse.document.documentID : "";
                    resp.createdEDMSDocumentId = createdDocumentID;

                    //handle setting of the property set
                    List<EdmsPropertySetRow> properties = new List<EdmsPropertySetRow>();
                    var row = new EdmsPropertySetRow();
                    row.fieldName = "Source";
                    row.fieldValue = "EMIS";
                    properties.Add(row);

                    //set the property set
                    //generate the property set
                    string xmlPropertySet = "<Propertysets><propertyset Name=\"" + Globals.EMIS_PROPERTY_SET_BT_FORM + "\"><propertyrow ";
                    foreach (var propSet in properties)
                    {
                        string fieldName = propSet.fieldName;
                        string fieldValue = propSet.fieldValue;
                        string propRow = " " + fieldName + "=\"" + fieldValue + "\" ";
                        xmlPropertySet += propRow;
                    }
                    xmlPropertySet += " /></propertyset></Propertysets>";


                    //attach the property set the document
                    XmlNode xmlRespAttachPropSet = edmsService.AddPropertySetRow(authenticationTicket, documentNameWithExtension, xmlPropertySet);
                    if (xmlRespAttachPropSet.Attributes["success"].Value != "true")
                    {
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR ADDING PROPERTY SET - " + xmlRespAttachPropSet.Attributes["error"].Value;
                        return resp;
                    }

                    //successfully attached the property set, now we update the document type
                    XmlNode xmlRespUpdateDocType = edmsService.UpdateDocumentType(authenticationTicket, documentNameWithExtension, Globals.EMIS_FORM_5_DOCTYPE_ID);
                    if (xmlRespUpdateDocType.Attributes["success"].Value != "true")
                    {
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR ADDING DOCUMENT TYPE - " + xmlRespUpdateDocType.Attributes["error"].Value;
                        return resp;
                    }
                    //attach document type end


                    //template create, send request to forward the document to a workflow

                    //if we are in debug mode, dont send to a workflow
                    if (Globals.IN_DEBUG_MODE == false)
                    {
                        SubmitDocumentToWorkflowRequest submitReq = new SubmitDocumentToWorkflowRequest();
                        submitReq.authenticationTicket = authenticationTicket;
                        submitReq.edmsFlowDefId = Globals.EMIS_WORK_FLOW_ID_BT_FORM;
                        submitReq.edmsFolderPath = IRNewDocumentPath + ".pdf";

                        var submitResp = SubmitDocumentToWorkflow(submitReq);

                        if (submitResp.statusCode != Globals.STATUS_CODE_SUCCESS)
                        {
                            resp.statusCode = Globals.STATUS_CODE_FAILED;
                            resp.statusDescription = "EDMS ERROR - The Document has been created using template, but error occurred on submitting to workflow [" + submitResp.statusDescription + "]";
                            return resp;
                        }
                    }

                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "Document successfully submitted to workflow.";
                    return resp;

                }
                else
                {
                    string error = xml_response.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR CREATING DOCUMENT FROM TEMPLATE - " + error;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        private static XmlDocument buildForm5XMLContent(CreateFormFiveFromTemplateRequest request)
        {

            System.Xml.XmlDocument dataContainerXml = new System.Xml.XmlDocument();
            System.Xml.XmlElement DataElem;

            dataContainerXml.LoadXml("<FORMDATA/>");

            //requisition ID
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "EmisRequisitionId");
            DataElem.InnerText = request.dtEmisRequisitionId;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //top header
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Supplies");
            DataElem.InnerText = request.dtProcType;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Finyear");
            DataElem.InnerText = request.dtFinancialYear;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "SeqNo");
            DataElem.InnerText = request.dtSequenceNumber;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //particulars of proc
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Subject");
            DataElem.InnerText = request.dtSubject;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "PlanRef");
            DataElem.InnerText = request.dtProcPlanRef;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Delivery");
            DataElem.InnerText = request.dtDeliveryLocation;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ReqDate");
            DataElem.InnerText = request.dtDateRequired;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //requestor
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "RequesterName");
            DataElem.InnerText = request.dtRequestorName;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "RequesterInitials");
            DataElem.InnerText = request.dtRequestorInitials;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "RequesterTitle");
            DataElem.InnerText = request.dtRequestorTitle;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "RequestDate");
            DataElem.InnerText = request.dtRequestorDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //hod
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ConfirmName");
            DataElem.InnerText = request.dtHodConfirmationName;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ConfirmInitials");
            DataElem.InnerText = request.dtHodConfirmationInitials;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ConfirmTitle");
            DataElem.InnerText = request.dtHodConfirmationTitle;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ConfirmDate");
            DataElem.InnerText = request.dtHodConfirmationDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Currency");
            DataElem.InnerText = request.dtCurrency;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "EstTotalCost");
            DataElem.InnerText = request.dtEstimatedTotalCost;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "AmountWords");
            DataElem.InnerText = request.dtAmountWords;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //build out the items
            for (int counter = 0; counter < request.items.Count && counter <= 19; counter++)
            {
                var data = request.items[counter];
                string prependIndex = (counter + 1).ToString("00");
                string nameItem = "Item" + prependIndex;
                string nameDesc = "Description" + prependIndex;
                string nameQty = "Qty" + prependIndex;
                string nameUnitOfMeasure = "Unit" + prependIndex;
                string nameEstUnitCost = "Est" + prependIndex;
                string nameMktPrice = "MarketPrice" + prependIndex;

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", nameItem);
                DataElem.InnerText = data.itemNo;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", nameDesc);
                DataElem.InnerText = data.description;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", nameQty);
                DataElem.InnerText = data.quantity;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", nameUnitOfMeasure);
                DataElem.InnerText = data.unitOfMeasure;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", nameEstUnitCost);
                DataElem.InnerText = data.estimatedUnitCost;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", nameMktPrice);
                DataElem.InnerText = data.marketPriceOfProcurement;
                dataContainerXml.DocumentElement.AppendChild(DataElem);


            }

            return dataContainerXml;

        }

        private BaseResponseEdmsDocument findDocumentByPath(srv edmsService, string authenticationTicket, string documentPath)
        {

            BaseResponseEdmsDocument resp = new BaseResponseEdmsDocument();

            //other data to be returned
            bool withPropertySets = true;
            bool withSecurity = true;
            bool withOwner = true;
            bool withVersions = true;

            XmlNode xmlResponse = edmsService.GetDocument(authenticationTicket, documentPath, withPropertySets, withSecurity, withOwner, withVersions);

            if (xmlResponse.Attributes["success"].Value != "true")
            {
                string error = xmlResponse.Attributes["error"].Value;
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = "EDMS ERROR - " + error;
                return resp;
            }

            //document details returned
            var nodeDocument = xmlResponse.FirstChild;

            if (nodeDocument == null)
            {
                string error = "Expected document node not found in response";
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = error;
                return resp;
            }

            //we got the node that has the document details
            string docId = getXMLAttributeValue(nodeDocument, "DocumentID", "");
            string docName = getXMLAttributeValue(nodeDocument, "Name", "");
            string docPath = getXMLAttributeValue(nodeDocument, "Path", "");

            EdmsDocument doc = new EdmsDocument();
            doc.documentID = docId;
            doc.documentName = docName;
            doc.documentPath = docPath;

            resp.document = doc;
            resp.statusCode = Globals.STATUS_CODE_SUCCESS;
            resp.statusDescription = Globals.STATUS_DESC_SUCCESS;
            return resp;
            
        }

        [HttpPost, Route("api/edmstools/new-bt-form-from-template")]
        public BaseResponse CreateBtFormUsingTemplate(CreateBtFormFromTemplateRequest request)
        {
            String createdDocumentID = "";
            BaseResponse resp = new BaseResponse();

            try
            {

                edms.srv edmsService = new edms.srv();

                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                string IRNewDocumentPath = request.newDocumentPath;
                string TemplatePath = Globals.EMIS_TEMPLATE_PATH_BT;
                XmlDocument dataContainerXml = buildBTFormTemplateBody(request);

                System.Xml.XmlNode xml_response;
                xml_response = edmsService.CreateDocumentUsingTemplate(authenticationTicket, IRNewDocumentPath, TemplatePath, dataContainerXml.OuterXml);
                // check response xml if any error occured.
                if (xml_response.Attributes["success"].Value.ToUpperInvariant() == "TRUE")
                {

                    string documentNameWithExtension = IRNewDocumentPath + ".pdf";
                    //get the document details from EDMS
                    BaseResponseEdmsDocument docResponse = findDocumentByPath(edmsService, authenticationTicket, documentNameWithExtension);
                    createdDocumentID = docResponse.statusCode == Globals.STATUS_CODE_SUCCESS ? docResponse.document.documentID : "";
                    resp.createdEDMSDocumentId = createdDocumentID;


                    //handle setting of the property set
                    List<EdmsPropertySetRow> properties = new List<EdmsPropertySetRow>();
                    var row = new EdmsPropertySetRow();
                    row.fieldName = "Source";
                    row.fieldValue = "EMIS";
                    properties.Add(row);

                    //generate the property set
                    string xmlPropertySet = "<Propertysets><propertyset Name=\"" + Globals.EMIS_PROPERTY_SET_BT_FORM + "\"><propertyrow ";
                    foreach (var propSet in properties)
                    {
                        string fieldName = propSet.fieldName;
                        string fieldValue = propSet.fieldValue;
                        string propRow = " " + fieldName + "=\"" + fieldValue + "\" ";
                        xmlPropertySet += propRow;
                    }
                    xmlPropertySet += " /></propertyset></Propertysets>";
                    
                    //attach the property set the document
                    XmlNode xmlRespAttachPropSet = edmsService.AddPropertySetRow(authenticationTicket, documentNameWithExtension, xmlPropertySet);
                    if (xmlRespAttachPropSet.Attributes["success"].Value != "true")
                    {
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR - ADDING PROPERTY SET " + xmlRespAttachPropSet.Attributes["error"].Value;
                        return resp;
                    }


                    //attach document type begin                    
                    //successfully attached the property set, now we update the document type
                    XmlNode xmlRespUpdateDocType = edmsService.UpdateDocumentType(authenticationTicket, documentNameWithExtension, Globals.EMIS_BT_FORM_DOCTYPE_ID);
                    if (xmlRespUpdateDocType.Attributes["success"].Value != "true")
                    {
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR - ADDING DOCUMENT TYPE " + xmlRespUpdateDocType.Attributes["error"].Value;
                        return resp;
                    }


                    //template create, send request to forward the document to a workflow

                    //if we are in debug mode, dont sent to a workflow
                    if (Globals.IN_DEBUG_MODE == false)
                    {
                        SubmitDocumentToWorkflowRequest submitReq = new SubmitDocumentToWorkflowRequest();
                        submitReq.authenticationTicket = authenticationTicket;
                        submitReq.edmsFlowDefId = Globals.EMIS_WORK_FLOW_ID_BT_FORM;
                        submitReq.edmsFolderPath = IRNewDocumentPath + ".pdf";

                        var submitResp = SubmitDocumentToWorkflow(submitReq);

                        if (submitResp.statusCode != Globals.STATUS_CODE_SUCCESS)
                        {
                            resp.statusCode = Globals.STATUS_CODE_FAILED;
                            resp.statusDescription = "EDMS ERROR - The Document has been created using template, but error occurred on submitting to workflow [" + submitResp.statusDescription + "]";
                            return resp;
                        }
                    }


                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "Document successfully submitted to workflow";
                    return resp;
                }
                else
                {
                    string error = xml_response.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR CREATING DOCUMENT FROM TEMPLATE - " + error;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        [HttpPost, Route("api/edmstools/new-bt-form-from-template-v1")]
        public BaseResponse CreateBtFormUsingTemplateV1(CreateBtFormFromTemplateRequest request)
        {
            String createdDocumentID = "";
            BaseResponse resp = new BaseResponse();

            try
            {

                edms.srv edmsService = new edms.srv();

                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                string IRNewDocumentPath = request.newDocumentPath;
                string TemplatePath = Globals.EMIS_TEMPLATE_PATH_BT;
                XmlDocument dataContainerXml = buildBTFormTemplateBodyV1(request);

                System.Xml.XmlNode xml_response;
                xml_response = edmsService.CreateDocumentUsingTemplate(authenticationTicket, IRNewDocumentPath, TemplatePath, dataContainerXml.OuterXml);
                // check response xml if any error occured.
                if (xml_response.Attributes["success"].Value.ToUpperInvariant() == "TRUE")
                {

                    string documentNameWithExtension = IRNewDocumentPath + ".pdf";
                    //get the document details from EDMS
                    BaseResponseEdmsDocument docResponse = findDocumentByPath(edmsService, authenticationTicket, documentNameWithExtension);
                    createdDocumentID = docResponse.statusCode == Globals.STATUS_CODE_SUCCESS ? docResponse.document.documentID : "";
                    resp.createdEDMSDocumentId = createdDocumentID;


                    //handle setting of the property set
                    List<EdmsPropertySetRow> properties = new List<EdmsPropertySetRow>();
                    var row = new EdmsPropertySetRow();
                    row.fieldName = "Source";
                    row.fieldValue = "EMIS";
                    properties.Add(row);

                    //generate the property set
                    string xmlPropertySet = "<Propertysets><propertyset Name=\"" + Globals.EMIS_PROPERTY_SET_BT_FORM + "\"><propertyrow ";
                    foreach (var propSet in properties)
                    {
                        string fieldName = propSet.fieldName;
                        string fieldValue = propSet.fieldValue;
                        string propRow = " " + fieldName + "=\"" + fieldValue + "\" ";
                        xmlPropertySet += propRow;
                    }
                    xmlPropertySet += " /></propertyset></Propertysets>";

                    //attach the property set the document
                    XmlNode xmlRespAttachPropSet = edmsService.AddPropertySetRow(authenticationTicket, documentNameWithExtension, xmlPropertySet);
                    if (xmlRespAttachPropSet.Attributes["success"].Value != "true")
                    {
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR - ADDING PROPERTY SET " + xmlRespAttachPropSet.Attributes["error"].Value;
                        return resp;
                    }


                    //attach document type begin                    
                    //successfully attached the property set, now we update the document type
                    XmlNode xmlRespUpdateDocType = edmsService.UpdateDocumentType(authenticationTicket, documentNameWithExtension, Globals.EMIS_BT_FORM_DOCTYPE_ID);
                    if (xmlRespUpdateDocType.Attributes["success"].Value != "true")
                    {
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR - ADDING DOCUMENT TYPE " + xmlRespUpdateDocType.Attributes["error"].Value;
                        return resp;
                    }


                    //template create, send request to forward the document to a workflow

                    //if we are in debug mode, dont sent to a workflow
                    if (Globals.IN_DEBUG_MODE == false)
                    {
                        SubmitDocumentToWorkflowRequest submitReq = new SubmitDocumentToWorkflowRequest();
                        submitReq.authenticationTicket = authenticationTicket;
                        submitReq.edmsFlowDefId = Globals.EMIS_WORK_FLOW_ID_BT_FORM;
                        submitReq.edmsFolderPath = IRNewDocumentPath + ".pdf";

                        var submitResp = SubmitDocumentToWorkflow(submitReq);

                        if (submitResp.statusCode != Globals.STATUS_CODE_SUCCESS)
                        {
                            resp.statusCode = Globals.STATUS_CODE_FAILED;
                            resp.statusDescription = "EDMS ERROR - The Document has been created using template, but error occurred on submitting to workflow [" + submitResp.statusDescription + "]";
                            return resp;
                        }
                    }


                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "Document successfully submitted to workflow";
                    return resp;
                }
                else
                {
                    string error = xml_response.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR CREATING DOCUMENT FROM TEMPLATE - " + error;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        private static XmlDocument buildBTFormTemplateBody(CreateBtFormFromTemplateRequest request)
        {

            System.Xml.XmlDocument dataContainerXml;
            System.Xml.XmlElement DataElem;
            dataContainerXml = new System.Xml.XmlDocument();
            dataContainerXml.LoadXml("<FORMDATA/>");

            //activities    
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Activity01");
            DataElem.InnerText = request.dtActivity1Desc;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "EstCost01");
            DataElem.InnerText = request.dtActivity1EstimatedCost;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Activity02");
            DataElem.InnerText = request.dtActivity2Desc;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "EstCost02");
            DataElem.InnerText = request.dtActivity2EstimatedCost;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Totals");
            DataElem.InnerText = request.dtTotalEstimatedCost;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //requested by
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Requester");
            DataElem.InnerText = request.dtRequestorName;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ReqInitials");
            DataElem.InnerText = request.dtRequestorInitials;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ReqDepartment");
            DataElem.InnerText = request.dtRequestorDepartment;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "RequestDate");
            DataElem.InnerText = request.dtRequestorDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //approved by HOD
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "HoDName");
            DataElem.InnerText = request.dtHodName;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "HoDDesignation");
            DataElem.InnerText = request.dtHodDesignation;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "HoDInitials");
            DataElem.InnerText = request.dtHodInitials;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "HoDDate");
            DataElem.InnerText = request.dtHodDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //to head of finance
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Date");
            DataElem.InnerText = request.dtHeadFinanceDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Claimant");
            DataElem.InnerText = request.dtHeadFinanceClaimant;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Designation");
            DataElem.InnerText = request.dtHeadFinanceDesignation;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Department");
            DataElem.InnerText = request.dtHeadFinanceDepartment;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimSubject");
            DataElem.InnerText = request.dtHeadFinanceClaimSubject;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //claimant
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantName");
            DataElem.InnerText = request.dtClaimantName;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantInitials");
            DataElem.InnerText = request.dtClaimantInitials;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimDate");
            DataElem.InnerText = request.dtClaimantDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "RecDepart");
            DataElem.InnerText = request.dtClaimantDepartment;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "claimantComment");
            DataElem.InnerText = request.dtClaimantComments;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "claimantComment");
            DataElem.InnerText = request.dtClaimantComments;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Total");
            DataElem.InnerText = request.dtTotal;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "AmountWords");
            DataElem.InnerText = request.dtAmountWords;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //build out the items
            for (int counter = 0; counter < request.items.Count && counter <= 29; counter++)
            {
                var data = request.items[counter];
                string prependIndex = (counter + 1) <= 10 ? ones((counter + 1).ToString()) : (counter + 1).ToString("00");
                string nameItem = (counter + 1) <= 10 ? prependIndex : "Item" + prependIndex;

                //for this column, 2 was mishandled in the pdf and html templates so we cater for it
                string nameParticular = "Part" + (prependIndex == "Two" ? "two" : prependIndex);
                string nameAmount = "Amount" + prependIndex;

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", nameItem);
                DataElem.InnerText = data.itemNo;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", nameParticular);
                DataElem.InnerText = data.particulars;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", nameAmount);
                DataElem.InnerText = data.amount;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

            }

            return dataContainerXml;
        }

        private static XmlDocument buildBTFormTemplateBodyV1(CreateBtFormFromTemplateRequest request)
        {

            System.Xml.XmlDocument dataContainerXml;
            System.Xml.XmlElement DataElem;
            dataContainerXml = new System.Xml.XmlDocument();
            dataContainerXml.LoadXml("<FORMDATA/>");

            //header fields requisition ID and Funding Source
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "SourceOfFunding");
            DataElem.InnerText = request.dtSourceOfFunding;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "EmisRequisitionId");
            DataElem.InnerText = request.dtEmisRequisitionId;
            dataContainerXml.DocumentElement.AppendChild(DataElem);


            //to head of finance
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Date");
            DataElem.InnerText = request.dtHeadFinanceDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Claimant");
            DataElem.InnerText = request.dtHeadFinanceClaimant;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Initials");
            DataElem.InnerText = request.dtHeadFinanceInitials;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Designation");
            DataElem.InnerText = request.dtHeadFinanceDesignation;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Department");
            DataElem.InnerText = request.dtHeadFinanceDepartment;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimSubject");
            DataElem.InnerText = request.dtHeadFinanceClaimSubject;
            dataContainerXml.DocumentElement.AppendChild(DataElem);


            //claimant
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantName");
            DataElem.InnerText = request.dtClaimantName;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantInitials");
            DataElem.InnerText = request.dtClaimantInitials;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimDate");
            DataElem.InnerText = request.dtClaimantDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "RecDepart");
            DataElem.InnerText = request.dtClaimantDepartment;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "claimantComment");
            DataElem.InnerText = request.dtClaimantComments;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "claimantComment");
            DataElem.InnerText = request.dtClaimantComments;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //approved by HOD
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "HodName");
            DataElem.InnerText = request.dtHodName;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "HodDepart");
            DataElem.InnerText = request.dtHodDepartment;
            dataContainerXml.DocumentElement.AppendChild(DataElem);
            
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "HodInitials");
            DataElem.InnerText = request.dtHodInitials;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "HodDate");
            DataElem.InnerText = request.dtHodDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "HodComment");
            DataElem.InnerText = request.dtHodComments;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //totals
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "Total");
            DataElem.InnerText = request.dtTotal;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "AmountWords");
            DataElem.InnerText = request.dtAmountWords;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            int countBudgetLines = 0;
            bool exceededMaxLinesAllowed = false;

            var dataGroups = request.dataGroups;

            foreach(var group in dataGroups)
            {
                //field names 
                string nameItem = "";
                string nameParticular = "";
                string nameAmount = "";
                string prependIndex = "";

                countBudgetLines = countBudgetLines + 1;
                if (countBudgetLines <= Globals.EDMS_BT_NUMBER_OF_BUDGET_LINES)
                {

                    //group header row i.e activity name

                    prependIndex = countBudgetLines <= 10 ? ones((countBudgetLines).ToString()) : (countBudgetLines).ToString("00");
                    nameItem = (countBudgetLines) <= 10 ? prependIndex : "Item" + prependIndex;                   
                    nameParticular = "Part" + (prependIndex == "Two" ? "two" : prependIndex);  //for this column, 2 was mishandled in the pdf and html templates so we cater for it
                    nameAmount = "Amount" + prependIndex;

                    DataElem = dataContainerXml.CreateElement("Prompt");
                    DataElem.SetAttribute("Name", nameParticular);
                    DataElem.InnerText = group.activityTitle;
                    dataContainerXml.DocumentElement.AppendChild(DataElem);

                    //handle the items
                    foreach (var item in group.items)
                    {

                        countBudgetLines = countBudgetLines + 1;
                        if(countBudgetLines > Globals.EDMS_BT_NUMBER_OF_BUDGET_LINES)
                        {
                            exceededMaxLinesAllowed = true;
                            break;
                        }
                        else
                        {
                            prependIndex = countBudgetLines <= 10 ? ones((countBudgetLines).ToString()) : (countBudgetLines).ToString("00");
                            nameItem = (countBudgetLines) <= 10 ? prependIndex : "Item" + prependIndex;
                            nameParticular = "Part" + (prependIndex == "Two" ? "two" : prependIndex);  //for this column, 2 was mishandled in the pdf and html templates so we cater for it
                            nameAmount = "Amount" + prependIndex;

                            DataElem = dataContainerXml.CreateElement("Prompt");
                            DataElem.SetAttribute("Name", nameItem);
                            DataElem.InnerText = item.itemNo;
                            dataContainerXml.DocumentElement.AppendChild(DataElem);

                            DataElem = dataContainerXml.CreateElement("Prompt");
                            DataElem.SetAttribute("Name", nameParticular);
                            DataElem.InnerText = item.particulars;
                            dataContainerXml.DocumentElement.AppendChild(DataElem);

                            DataElem = dataContainerXml.CreateElement("Prompt");
                            DataElem.SetAttribute("Name", nameAmount);
                            DataElem.InnerText = item.amount;
                            dataContainerXml.DocumentElement.AppendChild(DataElem);

                        }

                    }

                }
                else
                {
                    break; //max lines reached
                }

                if (exceededMaxLinesAllowed)
                {
                    break; //max lines reached
                }

                //insert the subtotal
                countBudgetLines = countBudgetLines + 2; //skipped a line for space
                if(countBudgetLines > Globals.EDMS_BT_NUMBER_OF_BUDGET_LINES)
                {
                    break;//exceeded bugdet line count
                }
                else
                {
                    prependIndex = countBudgetLines <= 10 ? ones((countBudgetLines).ToString()) : (countBudgetLines).ToString("00");
                    nameItem = (countBudgetLines) <= 10 ? prependIndex : "Item" + prependIndex;
                    nameParticular = "Part" + (prependIndex == "Two" ? "two" : prependIndex);  //for this column, 2 was mishandled in the pdf and html templates so we cater for it
                    nameAmount = "Amount" + prependIndex;

                    DataElem = dataContainerXml.CreateElement("Prompt");
                    DataElem.SetAttribute("Name", nameParticular);
                    DataElem.InnerText = "Sub Total: " + group.subTotal;
                    dataContainerXml.DocumentElement.AppendChild(DataElem);
                }


                //skip 1 line to create space
                countBudgetLines = countBudgetLines + 1;

            }

            return dataContainerXml;

        }


        private static XmlDocument buildBTFormMergedTemplateBody(CreateBtFormAuditMergedFromTemplateRequest request)
        {

            System.Xml.XmlDocument dataContainerXml;
            System.Xml.XmlElement DataElem;
            dataContainerXml = new System.Xml.XmlDocument();
            dataContainerXml.LoadXml("<FORMDATA/>");

            //header    
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "senderName");
            DataElem.InnerText = request.dtSenderName;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "senderDesign");
            DataElem.InnerText = request.dtSenderDesign;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "senderDept");
            DataElem.InnerText = request.dtSenderDept;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "claimDate");
            DataElem.InnerText = request.dtClaimDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "claimFor");
            DataElem.InnerText = request.dtClaimFor;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //Claimant
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantName");
            DataElem.InnerText = request.dtClaimantName;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantDept");
            DataElem.InnerText = request.dtClaimantDept;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantDate");
            DataElem.InnerText = request.dtClaimantDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantInitial");
            DataElem.InnerText = request.dtClaimantInitial;
            dataContainerXml.DocumentElement.AppendChild(DataElem);
            
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantRecom");
            DataElem.InnerText = request.dtClaimantRecom;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "ClaimantComments");
            DataElem.InnerText = request.dtClaimantComments;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //totals
            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "TotalAllowances");
            DataElem.InnerText = request.dtTotalAllowances;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "TotalAmountFuel");
            DataElem.InnerText = request.dtTotalAmountFuel;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "TotalAmountClaim");
            DataElem.InnerText = request.dtTotalAmountClaim;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "AmountWords");
            DataElem.InnerText = request.dtAmountWords;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            //
            //attach activity details
            //

            //activity 1
            if (request.activity1 != null)
            {
                dataContainerXml = attachActivityDetailsXml(dataContainerXml, request.activity1, 1);
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "activity1AmountTotal");
                DataElem.InnerText = request.activity1.staffPerdiemTotal;
                dataContainerXml.DocumentElement.AppendChild(DataElem);
            }

            //activity 2
            if (request.activity2 != null)
            {
                dataContainerXml = attachActivityDetailsXml(dataContainerXml, request.activity2, 1);
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "activity2AmountTotal");
                DataElem.InnerText = request.activity2.staffPerdiemTotal;
                dataContainerXml.DocumentElement.AppendChild(DataElem);
            }

            //activity 3
            if (request.activity3 != null)
            {
                dataContainerXml = attachActivityDetailsXml(dataContainerXml, request.activity3, 1);
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "activity3AmountTotal");
                DataElem.InnerText = request.activity3.staffPerdiemTotal;
                dataContainerXml.DocumentElement.AppendChild(DataElem);
            }

            //activity 4
            if (request.activity4 != null)
            {
                dataContainerXml = attachActivityDetailsXml(dataContainerXml, request.activity4, 1);
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "activity4AmountTotal");
                DataElem.InnerText = request.activity4.staffPerdiemTotal;
                dataContainerXml.DocumentElement.AppendChild(DataElem);
            }
            //activity 5
            if (request.activity5 != null)
            {
                dataContainerXml = attachActivityDetailsXml(dataContainerXml, request.activity5, 1);
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "activity5AmountTotal");
                DataElem.InnerText = request.activity5.staffPerdiemTotal;
                dataContainerXml.DocumentElement.AppendChild(DataElem);
            }
            //activity 6
            if (request.activity6 != null)
            {
                dataContainerXml = attachActivityDetailsXml(dataContainerXml, request.activity6, 1);
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "activity6AmountTotal");
                DataElem.InnerText = request.activity6.staffPerdiemTotal;
                dataContainerXml.DocumentElement.AppendChild(DataElem);
            }
            //activity 7
            if (request.activity7 != null)
            {
                dataContainerXml = attachActivityDetailsXml(dataContainerXml, request.activity7, 1);
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "activity7AmountTotal");
                DataElem.InnerText = request.activity7.staffPerdiemTotal;
                dataContainerXml.DocumentElement.AppendChild(DataElem);
            }
            //activity 8
            if (request.activity8 != null)
            {
                dataContainerXml = attachActivityDetailsXml(dataContainerXml, request.activity8, 1);
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "activity8AmountTotal");
                DataElem.InnerText = request.activity8.staffPerdiemTotal;
                dataContainerXml.DocumentElement.AppendChild(DataElem);
            }

            //
            //attach fuel details
            //

            //line 1
            if(request.fuelLine1 != null)
            {
                var data1 = request.fuelLine1;
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Fuel01");
                DataElem.InnerText = data1.description;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Litres01");
                DataElem.InnerText = data1.litres;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Rate01");
                DataElem.InnerText = data1.rate;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "TotalFuel01");
                DataElem.InnerText = data1.amount;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

            }
            //line 2
            if (request.fuelLine2 != null)
            {
                var data1 = request.fuelLine2;
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Fuel02");
                DataElem.InnerText = data1.description;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Litres02");
                DataElem.InnerText = data1.litres;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Rate02");
                DataElem.InnerText = data1.rate;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "TotalFuel02");
                DataElem.InnerText = data1.amount;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

            }
            //line 3
            if (request.fuelLine3 != null)
            {
                var data1 = request.fuelLine3;
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Fuel03");
                DataElem.InnerText = data1.description;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Litres03");
                DataElem.InnerText = data1.litres;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Rate03");
                DataElem.InnerText = data1.rate;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "TotalFuel03");
                DataElem.InnerText = data1.amount;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

            }
            //line 4
            if (request.fuelLine4 != null)
            {
                var data1 = request.fuelLine4;
                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Fuel04");
                DataElem.InnerText = data1.description;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Litres04");
                DataElem.InnerText = data1.litres;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "Rate04");
                DataElem.InnerText = data1.rate;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

                DataElem = dataContainerXml.CreateElement("Prompt");
                DataElem.SetAttribute("Name", "TotalFuel04");
                DataElem.InnerText = data1.amount;
                dataContainerXml.DocumentElement.AppendChild(DataElem);

            }
            
            return dataContainerXml;

        }

        private static XmlDocument attachActivityDetailsXml(XmlDocument dataContainerXml, ActivityDetail activity, int activityNo)
        {

            String activityNum = activityNo.ToString();
            String activityNumWithZero = activityNo.ToString("00");

            XmlElement DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "pde"+ activityNumWithZero);
            DataElem.InnerText = activity.teamOrPde;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "start"+ activityNumWithZero);
            DataElem.InnerText = activity.startDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "end" + activityNumWithZero);
            DataElem.InnerText = activity.endDate;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            DataElem = dataContainerXml.CreateElement("Prompt");
            DataElem.SetAttribute("Name", "days" + activityNumWithZero);
            DataElem.InnerText = activity.noOfDays;
            dataContainerXml.DocumentElement.AppendChild(DataElem);

            int countStaff = activity.staffPerdiem.Count;
            for (int i = 0; i < 5; i++)
            {
                if (i < countStaff)
                {
                    var data = activity.staffPerdiem[i];

                    string activityString = "activity"+activityNum;
                    string prependIndex = (i + 1).ToString("00");
                    string nameStaff = activityString + "Staff" + prependIndex;
                    string namePerdiem = activityString + "Perdiem" + prependIndex;
                    string nameAmount = activityString + "Amount" + prependIndex;

                    DataElem = dataContainerXml.CreateElement("Prompt");
                    DataElem.SetAttribute("Name", nameStaff);
                    DataElem.InnerText = data.staff;
                    dataContainerXml.DocumentElement.AppendChild(DataElem);

                    DataElem = dataContainerXml.CreateElement("Prompt");
                    DataElem.SetAttribute("Name", namePerdiem);
                    DataElem.InnerText = data.perdiem;
                    dataContainerXml.DocumentElement.AppendChild(DataElem);

                    DataElem = dataContainerXml.CreateElement("Prompt");
                    DataElem.SetAttribute("Name", nameAmount);
                    DataElem.InnerText = data.amount;
                    dataContainerXml.DocumentElement.AppendChild(DataElem);

                }
            }

            return dataContainerXml;
        }

        private static String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
                case 10:
                    name = "Ten";
                    break;
            }
            return name;
        }


        //[HttpPost, Route("api/edmstools/submit-document-to-workflow")]
        internal BaseResponse SubmitDocumentToWorkflow(SubmitDocumentToWorkflowRequest request)
        {

            BaseResponse resp = new BaseResponse();
            try
            {

                edms.srv edmsService = new edms.srv();

                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket = request.authenticationTicket;             
                string workflowID = request.edmsFlowDefId;
                string documentPath = request.edmsFolderPath;

                XmlNode xmlResponse;
                xmlResponse = edmsService.SubmitDocumentToFlow(authenticationTicket, documentPath, workflowID);

                if (xmlResponse.Attributes["success"].Value == "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "Document successfully submitted to Workflow";
                    return resp;
                }
                else
                {
                    string error = xmlResponse.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        [HttpPost, Route("api/edmstools/rename-document")]
        public BaseResponse RenameDocument(RenameDocumentRequest request)
        {

            BaseResponse resp = new BaseResponse();
            try
            {

                edms.srv edmsService = new edms.srv();

                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                string path = request.currentDocumentPath; 
                string newDocumentName = request.newDocumentName;
                string description = string.IsNullOrEmpty(request.newDescription) ? "Rename Document by Edms Tools API" : request.newDescription;
                string updateInstructions = "";

                XmlNode xmlResponse;
                xmlResponse = edmsService.UpdateDocumentProperties(authenticationTicket,path,newDocumentName, description, updateInstructions);

                if (xmlResponse.Attributes["success"].Value == "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "Document successfully renamed";
                    return resp;
                }
                else
                {
                    string error = xmlResponse.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        [HttpPost, Route("api/edmstools/download-document")]
        public BaseResponse DownloadDocumentFromEdmsServer(DownloadDocumentRequest request)
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;

                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                XmlNode xmlResponse;
                edms.srv edmsService = new edms.srv();

                byte[] documentByteArray = null;

                //if below returns zero bytes, then call zero (0) bytes then call the function "GetDownloadInfo" to get the error description.
                documentByteArray = edmsService.DownloadDocument(authenticationTicket, request.edmsDocumentPath);

                if (documentByteArray == null || documentByteArray.Length == 0)
                {
                    //something went wrong on the call to download
                    xmlResponse = edmsService.GetDownloadInfo(authenticationTicket, request.edmsDocumentPath);

                    if (xmlResponse.Attributes["success"].Value == "true")
                    {
                        string error = xmlResponse.Attributes["error"].Value;
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR - " + error;
                        return resp;
                    }
                    else
                    {
                        resp.statusCode = "1";
                        string error = xmlResponse.Attributes["error"].Value;
                        resp.statusDescription = "Failed to retrieve download error [" + error + "]";
                        return resp;
                    }

                }
                
                //convert the byte to a string to send in the response
                string base64DocContent = Convert.ToBase64String(documentByteArray, Base64FormattingOptions.InsertLineBreaks);

                resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                resp.statusDescription = "Document download successful";
                resp.result = base64DocContent;
                return resp;

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        [HttpPost, Route("api/edmstools/download-document-by-id")]
        public BaseResponse DownloadDocumentByDocumentIdFromEdmsServer(DownloadDocumentByIdRequest request)
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;

                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                XmlNode xmlResponse;
                edms.srv edmsService = new edms.srv();


                //get the document path using the document ID
                BaseResponse docSearchResp = findDocumentById(edmsService, authenticationTicket, request.documentId);

                //check if we got the response
                if(docSearchResp.statusCode != Globals.STATUS_CODE_SUCCESS)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = docSearchResp.statusDescription;
                    return resp;
                }

                //we got the document
                string edmsDocumentPath =  docSearchResp.statusDescription;

                //we now attempt the download using the document path
                byte[] documentByteArray = null;

                //if below returns zero bytes, then call zero (0) bytes then call the function "GetDownloadInfo" to get the error description.
                documentByteArray = edmsService.DownloadDocument(authenticationTicket,edmsDocumentPath);

                if (documentByteArray == null || documentByteArray.Length == 0)
                {
                    //something went wrong on the call to download
                    xmlResponse = edmsService.GetDownloadInfo(authenticationTicket, edmsDocumentPath);

                    if (xmlResponse.Attributes["success"].Value == "true")
                    {
                        string error = xmlResponse.Attributes["error"].Value;
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "EDMS ERROR - " + error;
                        return resp;
                    }
                    else
                    {
                        resp.statusCode = "1";
                        string error = xmlResponse.Attributes["error"].Value;
                        resp.statusDescription = "Failed to retrieve download error [" + error + "]";
                        return resp;
                    }

                }

                //convert the byte to a string to send in the response
                string base64DocContent = Convert.ToBase64String(documentByteArray, Base64FormattingOptions.InsertLineBreaks);

                resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                resp.statusDescription = "Document download successful";
                resp.result = base64DocContent;
                return resp;

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        [HttpPost, Route("api/edmstools/search-documents")]
        public BaseResponse SearchForDocumentByDocIdFromEdmsServer(DocumentSearchByIdRequest request)
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;

                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                srv edmsService = new srv();
                resp = findDocumentById(edmsService, authenticationTicket, request.documentId);
                return resp;

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        private static BaseResponse findDocumentById(srv edmsService, string authenticationTicket, string documentID)
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                if (!prepareSearch(edmsService, authenticationTicket, documentID))
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "FAILED TO PREPARE SEARCH";
                    return resp;
                }

                string returnedDocumentPath = ""; //variable to hold the document path
                bool lastPage = false;
                while (lastPage == false)
                {
                    nextPage(edmsService, authenticationTicket, out lastPage, out returnedDocumentPath);
                }

                if (returnedDocumentPath == "")
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "DOCUMENT NOT FOUND";
                    return resp;
                }

                resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                resp.statusDescription = returnedDocumentPath;
                return resp;

            }
            catch(Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = "EXCEPTION OCCURED ["+ex.Message+"]";
                return resp;
            }
          
        }

        [HttpPost, Route("api/edmstools/search-documents-by-path")]
        public BaseResponseEdmsDocument SearchForDocumentByDocPathFromEdmsServer(DocumentSearchByPathRequest request)
        {

            BaseResponseEdmsDocument resp = new BaseResponseEdmsDocument();

            try
            {

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                edms.srv edmsService = new edms.srv();
                resp = findDocumentByPath(edmsService, null, request.edmsDocumentPath);
                return resp;

                //string authenticationTicket;

                //request.authenticationTicket = "";
                //if (string.IsNullOrEmpty(request.authenticationTicket))
                //{

                //    LoginRequest loginRequest = new LoginRequest();
                //    loginRequest.username = Globals.API_USERNAME;
                //    loginRequest.password = Globals.API_PASSWORD;

                //    BaseResponse loginResponse = Login(loginRequest);
                //    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                //    {
                //        resp.statusCode = loginResponse.statusCode;
                //        resp.statusDescription = loginResponse.statusDescription;
                //        return resp;
                //    }

                //    //we managed to get an authentication ticket
                //    authenticationTicket = loginResponse.result;
                //}
                //else
                //{
                //    authenticationTicket = request.authenticationTicket;
                //}

                //edms.srv edmsService = new edms.srv();

                //string documentID = ""; 
                //string documentPath = request.edmsDocumentPath;
                //if (!prepareSearch(edmsService, authenticationTicket, documentID, documentPath))
                //{
                //    resp.statusCode = Globals.STATUS_CODE_FAILED;
                //    resp.statusDescription = "FAILED TO PREPARE SEARCH";
                //    return resp;
                //}

                //bool lastPage = false;
                //while (lastPage == false)
                //{
                //    nextPage(edmsService, authenticationTicket, out lastPage, out documentPath);
                //}

                //resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                //resp.statusDescription = "Search Complete";
                //return resp;

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }
        
        [HttpPost, Route("api/edmstools/get-document")]
        public BaseResponse GetDocumentByPath(GetDocumentByPathRequest request)
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                request.edmsDocumentPath = "/PPDA DEPARTMENTAL WORKSPACES/EMIS/Letters/james.pdf";

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;

                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                XmlNode xmlResponse;
                edms.srv edmsService = new edms.srv();

                //something went wrong on the call to download
                bool withPropertySets = true;
                bool withSecurity = true;
                bool withOwner = true;
                bool withVersions = true;

                xmlResponse = edmsService.GetDocument(
                    authenticationTicket, request.edmsDocumentPath, withPropertySets,withSecurity, withOwner,  withVersions);

                if (xmlResponse.Attributes["success"].Value != "true")
                {
                    string error = xmlResponse.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }
                else
                {

                    var nodeDocument = xmlResponse.FirstChild;

                    if(nodeDocument != null)
                    {
                        string docId = getXMLAttributeValue(nodeDocument, "DocumentID", "");
                        string docName = getXMLAttributeValue(nodeDocument, "Name", "");
                        string docPath = getXMLAttributeValue(nodeDocument, "Path", "");

                        //foreach(XmlElement node in nodeDocument)
                        //{

                        //    if (node.Name.ToLower() == "name")
                        //    {

                        //    }

                        //}

                    }

                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = Globals.STATUS_DESC_SUCCESS;
                    return resp;

                }                               

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        private BaseResponse testDocumentRequest()
        {
            BaseResponse resp = new BaseResponse();
            resp.statusCode = Globals.STATUS_CODE_SUCCESS;
            resp.statusDescription = Globals.STATUS_DESC_SUCCESS;
            resp.result = Globals.DUMMY_DOC_CONTENT;
            return resp;
        }

        private void saveDocumentToLocalFile(string documentContentsBase64String)
        {
            //get bytes from Base64 string
            byte[] bytes = System.Convert.FromBase64String(documentContentsBase64String);

            string timeNow = DateTime.Now.Ticks + ".pdf";
            string pathToSaveFile = @"C:\Users\Leontymo\Desktop\Delete\" + timeNow;
            File.WriteAllBytes(pathToSaveFile, bytes);
        }


        static bool prepareSearch(edms.srv edmsServer, string ticket, string documentID, string documentPath = "")
        {

            string FOLDER = "/PPDA DEPARTMENTAL WORKSPACES/EMIS/Letters/";

            string searchCriteria =  "<SEARCH>" +
                                     "<ITEM NAME=\"SEARCHSCOPE\" VALUE=\"ONLINE\" />" +
                                     "<ITEM NAME=\"DOCUMENTID\" VALUE=\""+ documentID + "\" />" +
                                     //"<ITEM NAME=\"FOLDER\" VALUE=\"" + xmlEncode(FOLDER) + "\" />" +
                                     "<ITEM NAME=\"SEARCHFOR\" VALUE=\"DOCUMENTSONLY\" />" +
                                     "</SEARCH>";

            XmlNode xResponse = edmsServer.Search(ticket, searchCriteria, "DOCUMENTNAME", true);
            if (getXMLAttributeValue(xResponse, "success", "").ToUpperInvariant() == "TRUE")
            {
                //writestatus("Search prepared.");
                return true;
            }
            else
            {
                // writestatus("Search cannot be prepared.");
                string error = getXMLAttributeValue(xResponse, "error", "");
                return false;
            }

        }

        static bool nextPage(edms.srv edms, string ticket, out bool lastpage, out string returnedDocumentPath)
        {

            returnedDocumentPath = "";
            lastpage = true;

            XmlNode xResponse = edms.GetNextSearchPage(ticket, false, false, false, false, false);
            if (getXMLAttributeValue(xResponse, "success", "").ToUpperInvariant() != "TRUE")
            {
                string error = getXMLAttributeValue(xResponse, "error", "");
                return false;
            }

            lastpage = (getXMLAttributeValue(xResponse, "LastPage", "").ToUpperInvariant() == "TRUE");

            foreach (XmlElement xmlitem in xResponse)
            {

                if (xmlitem.Name.ToUpperInvariant() == "DOCUMENT")
                {
                    string docpath = getXMLAttributeValue(xmlitem, "Path", "");
                    string docname = getXMLAttributeValue(xmlitem, "Name", "");
                    int docid = Convert.ToInt32(getXMLAttributeValue(xmlitem, "DocumentID", "0"));
                    string fullFilePath = docpath.Replace("\\", "/") + "/" + docname;

                    returnedDocumentPath = fullFilePath;
                    lastpage = true; //to ensure we stop the search
                    break;
                }

            }

            xResponse = null;
            return true;
        }


        static string xmlEncode(string xmlstr)
        {
            xmlstr = xmlstr.Replace("&", "&amp;");
            xmlstr = xmlstr.Replace("<", "&lt;");
            xmlstr = xmlstr.Replace(">", "&gt;");
            xmlstr = xmlstr.Replace("'", "&apos;");
            xmlstr = xmlstr.Replace("\"", "&quot;");
            return xmlstr;
        }

        static string getXMLAttributeValue(XmlNode xNode, string AttrName, string defaultvalue)
        {
            if (xNode == null) { return defaultvalue; }
            if (xNode.Attributes[AttrName] == null) { return defaultvalue; }
            string xValue = xNode.Attributes[AttrName].Value;
            if (xValue == null) { return defaultvalue; }
            xValue = xValue.Trim();
            if (xValue.Length == 0) { return defaultvalue; }
            return xValue;
        }

        [HttpPost, Route("api/edmstools/delete-document")]
        public BaseResponse DeleteDocument(DeleteDocumentRequest request)
        {

            BaseResponse resp = new BaseResponse();

            try
            {                

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;

                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                XmlNode xmlResponse;
                edms.srv edmsService = new edms.srv();
               
                xmlResponse = edmsService.DeleteDocument(authenticationTicket, request.edmsDocumentPath);

                if (xmlResponse.Attributes["success"].Value != "true")
                {
                    string error = xmlResponse.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }
                else
                {                 
                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = Globals.STATUS_DESC_SUCCESS;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        [HttpPost, Route("api/edmstools/update-document-type")]
        public BaseResponse UpdateDocumentType(UpdateDocumentTypeRequest request)
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                edms.srv edmsService = new edms.srv();

                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }
                

                XmlNode xmlResponse;
                xmlResponse = edmsService.UpdateDocumentType(authenticationTicket, request.edmsDocumentPath, request.documentTypeId);

                if (xmlResponse.Attributes["success"].Value == "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "The Document type has been successfully updated";
                    return resp;
                }
                else
                {
                    string error = xmlResponse.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        [HttpGet, Route("api/edmstools/test")]
        public BaseResponse testCall()
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                edms.srv edmsService = new edms.srv();

                //get authentication ticket
                string authenticationTicket = "";
                if (string.IsNullOrEmpty(authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }


                //get the document + property set by it's path
                XmlNode xmlResponse;
                string path = "/PPDA DEPARTMENTAL WORKSPACES/EMIS/BT_Form_Performance Monitoring_Sharon Nassaza_October 29 2019.pdf";
                int docTypeId = Globals.DOCUMENT_TYPE_ID_EMIS_ATTACHMENT;
                resp = attachDocumentTypeToEdmsDoc(edmsService, authenticationTicket, path, docTypeId);

                return resp;



                /*
                 OLD LOGIC
                 */

                bool returnPropertySet = true;
                xmlResponse = edmsService.GetDocument(authenticationTicket, path, returnPropertySet, false, false, false);

                string oldEmisAttachmentType = "";
                string oldEmisAttachmentTypeRowNumber = "";

                if (xmlResponse.Attributes["success"].Value == "true")
                {

                    var nodeDocument = xmlResponse.FirstChild;

                    if (nodeDocument != null)
                    {

                        XmlNodeList nodeList = nodeDocument.ChildNodes;
                        foreach (XmlNode node in nodeList)
                        {

                            if (node.Name == "Propertysets")
                            {
                                var propertySetNode = node.FirstChild;

                                foreach (XmlNode propertyRowNode in propertySetNode.ChildNodes)
                                {
                                    var emisAttachmentType = getXMLAttributeValue(propertyRowNode, "EMIS_ATTACHMENT_TYPE", "");
                                    var emisRowNbr = getXMLAttributeValue(propertyRowNode, "RowNbr", "");
                                    oldEmisAttachmentType = emisAttachmentType;
                                    oldEmisAttachmentTypeRowNumber = emisRowNbr;
                                }

                            }

                        }


                    }

                    if (!string.IsNullOrEmpty(oldEmisAttachmentTypeRowNumber))
                    {

                        string newEmisAttachmentType = "UpdatedAttachmentType";
                        //update the field
                        int rowId = 0;
                        int.TryParse(oldEmisAttachmentTypeRowNumber, out rowId);
                        string xmlPropertySet =
                            "<Propertysets>" +
                                "<propertyset Name=\"EMIS_ATTACHMENTS\">" +
                                "<propertyrow RowNbr=\"" + rowId + "\" EMIS_ATTACHMENT_TYPE=\"" + newEmisAttachmentType + "\" />" +
                                "</propertyset>" +
                            "</Propertysets>";


                        XmlNode xmlNodeUpdateResp = edmsService.UpdatePropertySetRow(authenticationTicket, path, xmlPropertySet);


                        if (xmlNodeUpdateResp.Attributes["success"].Value == "true")
                        {
                            resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                            resp.statusDescription = "Property set row successfully updated";
                            return resp;
                        }
                        else
                        {
                            string error = xmlNodeUpdateResp.Attributes["error"].Value;
                            resp.statusCode = Globals.STATUS_CODE_FAILED;
                            resp.statusDescription = "EDMS ERROR - " + error;
                            return resp;
                        }


                        resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                        resp.statusDescription = xmlResponse.OuterXml;
                        return resp;
                    }

                }
                else
                {
                    string error = xmlResponse.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }

                resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                resp.statusDescription = "DONE";
                return resp;

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        [HttpGet, Route("api/edmstools/test1")]        
        public BaseResponseDocumentTasks test1Call()
        {

            BaseResponseDocumentTasks resp = new BaseResponseDocumentTasks();

            try
            {

                edms.srv edmsService = new edms.srv();

                //get authentication ticket
                string authenticationTicket = "";
                if (string.IsNullOrEmpty(authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }

                int documentID = 38101;
                BaseResponseDocumentTasks respTasks = checkIfFinalStepOfRequisitionIsCompleted(edmsService, authenticationTicket, documentID);
                return respTasks;

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        private BaseResponseDocumentTasks checkIfFinalStepOfRequisitionIsCompleted(srv edmsService, string authenticationTicket, int documentID)
        {

            BaseResponseDocumentTasks resp = new BaseResponseDocumentTasks();

            try
            {

                string sortBy = "0";
                bool ascendingOrder = true;

                string xmlCriteriaData =
                    "<CRITERIA>" +
                        "<ITEM NAME=\"DOCUMENTID\" VALUE=\"" + documentID + "\" />" +
                    "</CRITERIA>";

                XmlNode xmlNodeTasksResp = edmsService.getTasks(authenticationTicket, xmlCriteriaData, sortBy, ascendingOrder);
                if (xmlNodeTasksResp.Attributes["success"].Value != "true")
                {
                    string error = xmlNodeTasksResp.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }

                //we got the tasks
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlNodeTasksResp.OuterXml);

                XmlNodeList taskNodes = xmlDoc.SelectNodes("/response/tasks/Task");

                List<EdmsTask> taskList = new List<EdmsTask>();

                foreach (XmlNode taskNode in taskNodes)
                {
                    EdmsTask task = formatXmlTaskToEdmsTask(taskNode);
                    taskList.Add(task);
                }

                List<EdmsTask> SortedList = taskList.OrderBy(o => o.stepNumber).ToList();

                //get the last step in the workflow
                EdmsTask lastEdmsTask = SortedList[SortedList.Count - 1];

                EdmsDocumentWithCompletedWorkflow completedDocument = checkForWorkflowCompletion(SortedList, documentID.ToString());

                if(completedDocument == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "Final Task not yet Completed";                  
                    return resp;
                }
                
                resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                resp.statusDescription = "Final Task has been Completed";
                resp.tasks = SortedList;
                return resp;

            }
            catch (Exception ex)
            {
                string error = ex.Message;
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = "Exception occurred [" + ex.Message + "]";
                return resp;
            }

        }

        private EdmsDocumentWithCompletedWorkflow checkForWorkflowCompletion(List<EdmsTask> taskList, string edmsDocId)
        {

            if(taskList.Count == 0)
            {
                return null;
            }

            //sort the by Step Number
            List<EdmsTask> SortedList = taskList.OrderBy(o => o.stepNumber).ToList();

            //get the last step in the workflow
            EdmsTask lastEdmsTask = SortedList[SortedList.Count - 1];

            if(lastEdmsTask.taskStatus != Globals.EDMS_TASK_STATUS_COMPLETED)
            {
                return null;
            }

            //status of last task is completed
            EdmsDocumentWithCompletedWorkflow completedDoc = new EdmsDocumentWithCompletedWorkflow();
            completedDoc.edmsDocumentId = edmsDocId;
            completedDoc.completedBy = lastEdmsTask.taskOwner;
            completedDoc.completionDate = lastEdmsTask.finishedOn;
            completedDoc.approvalStatus = lastEdmsTask.approvalStatus;

            return completedDoc;

        }

        private BaseResponseDocumentTasks getEDMSWorkflowStatus(srv edmsService, string authenticationTicket, int documentID)
        {

            BaseResponseDocumentTasks resp = new BaseResponseDocumentTasks();

            try
            {              

                string sortBy = "0";
                bool ascendingOrder = true;

                string xmlCriteriaData =
                    "<CRITERIA>" +
                        "<ITEM NAME=\"DOCUMENTID\" VALUE=\"" + documentID + "\" />" +
                    "</CRITERIA>";

                XmlNode xmlNodeTasksResp = edmsService.getTasks(authenticationTicket, xmlCriteriaData, sortBy, ascendingOrder);
                if (xmlNodeTasksResp.Attributes["success"].Value != "true")
                {
                    string error = xmlNodeTasksResp.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }

                //we got the tasks
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xmlNodeTasksResp.OuterXml);

                XmlNodeList taskNodes = xmlDoc.SelectNodes("/response/tasks/Task");

                List<EdmsTask> taskList = new List<EdmsTask>();

                foreach (XmlNode taskNode in taskNodes)
                {
                    EdmsTask task = formatXmlTaskToEdmsTask(taskNode);
                    taskList.Add(task);
                }

                List<EdmsTask> SortedList = taskList.OrderBy(o => o.stepNumber).ToList();

                resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                resp.statusDescription = Globals.STATUS_DESC_SUCCESS;
                resp.tasks = SortedList;
                return resp;

            }
            catch(Exception ex)
            {
                string error = ex.Message;
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = "Exception occurred [" + ex.Message + "]";
                return resp;
            }

        }

        private static EdmsTask formatXmlTaskToEdmsTask(XmlNode taskNodea)
        {

            var task = new EdmsTask();

            XmlNodeList taskAttributes = taskNodea.ChildNodes;
            foreach (XmlNode nodeAttribute in taskAttributes)
            {
                string nodeName = nodeAttribute.Name;
                if (nodeName == "StepNumber")
                {
                    int no = 0;
                    task.stepNumber = int.TryParse(nodeAttribute.InnerText, out no)? no : 0;
                }
                else if (nodeName == "StepName")
                {
                    task.stepName = nodeAttribute.InnerText;
                }
                else if (nodeName == "TaskName")
                {
                    task.taskName = nodeAttribute.InnerText;
                }
                else if (nodeName == "AssigneeName")
                {
                    task.taskOwner = nodeAttribute.InnerText;
                }
                else if (nodeName == "AssignedByName")
                {
                    task.assignedBy = nodeAttribute.InnerText;
                }
                else if (nodeName == "TaskStatus")
                {

                    string status = nodeAttribute.InnerText;
                    if(status == "NotStarted")
                    {
                        status = "Not Started";
                    }else if (status == "InProgress")
                    {
                        status = "Pending";
                    }
                    else if (status == "Completed")
                    {
                        status = "Task Completed";
                    }
                    task.taskStatus = status;

                }
                else if (nodeName == "StartDtae")
                {
                    task.startDate = nodeAttribute.InnerText;
                }
                else if (nodeName == "FinishDate")
                {
                    task.finishedOn = nodeAttribute.InnerText;
                }
                else if (nodeName == "DueDate")
                {
                    task.dueDate = nodeAttribute.InnerText;
                }
                else if (nodeName == "StartVersionNumber")
                {
                    task.assignedVersion = nodeAttribute.InnerText;
                }
                else if (nodeName == "EndVersionNumber")
                {
                    task.endVersion = nodeAttribute.InnerText;
                }
                else if (nodeName == "ApprovalStatus")
                {
                    task.approvalStatus = nodeAttribute.InnerText;
                }

            }

            return task;
        }

        internal BaseResponse attachDocumentTypeToEdmsDoc(edms.srv edmsService,string authenticationTicket,string edmsDocPath, int documentTypeID)
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                //1. get the document types from EDMS (GetDocumentTypes) -
                //2. find the document type with the [documentTypeID], if not found return error -
                //3. get the property set name and property set ID for the property set attached to the document -
                //4. get the property row fields for the property set (GetPropertySetDefinition) -
                //5. add the property set to the document using the fields above as property rows (AddPropertySetRow)
                //6. update the document type of the document

                XmlNode xmlRespDocTypes = edmsService.GetDocumentTypes(authenticationTicket);

                if (xmlRespDocTypes.Attributes["success"].Value != "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = xmlRespDocTypes.Attributes["error"].Value;
                    return resp;
                }

                XmlDocument doc = new XmlDocument();

                doc.LoadXml(xmlRespDocTypes.OuterXml);
                XmlNodeList docTypeNodes = doc.SelectNodes("/response/DocumentTypes/DocumentType");

                string propertySetName = "";
                string propertySetID = "";
                foreach (XmlNode docTypeNode in docTypeNodes)
                {
                    string retrievedDocTypeId = getXMLAttributeValue(docTypeNode, "TypeID", "");
                    if(retrievedDocTypeId == documentTypeID.ToString())
                    {
                        //we found the document type
                        propertySetName = getXMLAttributeValue(docTypeNode, "PropertySetName", "");
                        propertySetID = getXMLAttributeValue(docTypeNode, "PropertySetID", "");
                        break; //get out of the loop quickly
                    }
                }

                //check if we have the property set
                if (string.IsNullOrEmpty(propertySetName))
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "No property set attached to Document Type with ID ["+documentTypeID+"]";
                    return resp;
                }


                //get the fields for property set
                XmlNode xmlRespPropSetDef = edmsService.GetPropertySetDefinition(authenticationTicket, propertySetName);

                if (xmlRespPropSetDef.Attributes["success"].Value != "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = xmlRespPropSetDef.Attributes["error"].Value;
                    return resp;
                }

                doc.LoadXml(xmlRespPropSetDef.OuterXml);
                XmlNodeList fieldNodes = doc.SelectNodes("/response/PropertySet/Fields/field");
                List<string> fields = new List<string>();
                foreach(XmlNode node in fieldNodes)
                {
                    string fieldName = getXMLAttributeValue(node, "FieldName", "");
                    fields.Add(fieldName);
                }


                string xmlPropertySet = "<Propertysets><propertyset Name=\"" + propertySetName + "\">";
                foreach(var field in fields)
                {
                    string fieldValue = "NA";
                    string propRow = "<propertyrow "+field+"=\"" + fieldValue + "\" />";
                    xmlPropertySet += propRow;
                }                                               
                xmlPropertySet += "</propertyset></Propertysets>";


                //attach the property set the document
                XmlNode xmlRespAttachPropSet = edmsService.AddPropertySetRow(authenticationTicket, edmsDocPath, xmlPropertySet);

                if (xmlRespAttachPropSet.Attributes["success"].Value != "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = xmlRespAttachPropSet.Attributes["error"].Value;
                    return resp;
                }


                //successfully attached the property set, now we update the document type
                XmlNode xmlRespUpdateDocType;
                xmlRespUpdateDocType = edmsService.UpdateDocumentType(authenticationTicket, edmsDocPath, documentTypeID);

                if (xmlRespUpdateDocType.Attributes["success"].Value == "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "The Document type has been successfully updated";
                    return resp;
                }
                else
                {
                    string error = xmlRespUpdateDocType.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }              

            }catch(Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        [HttpPost, Route("api/edmstools/upload-document-with-properties")]
        public BaseResponse AddEdmsDocumentWithProperties(UploadDocumentWithPropertiesRequest request)
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                //var timeNow = DateTime.Now;
                //timeNow = timeNow.AddMinutes(5);
                //while(DateTime.Now < timeNow)
                //{
                //    int x = 0;
                //    x++;
                //}

                edms.srv edmsService = new edms.srv();

                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                string targetEdmsPath = request.targetEdmsPath;
                string documentContentsBase64String = request.documentContents;

                //get bytes from Base64 string
                byte[] bytes = System.Convert.FromBase64String(documentContentsBase64String);

                XmlNode xmlResponse;
                xmlResponse = edmsService.UploadDocument(authenticationTicket, targetEdmsPath, bytes);

                //error occurred on uploading the document
                if (xmlResponse.Attributes["success"].Value != "true")
                {
                    string error = xmlResponse.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;                    
                }


                //document successfully updated, now we go on. 1. setting the property set, 2.setting the document type

                XmlNode xmlRespDocTypes = edmsService.GetDocumentTypes(authenticationTicket);
                if (xmlRespDocTypes.Attributes["success"].Value != "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = xmlRespDocTypes.Attributes["error"].Value;
                    return resp;
                }


                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlRespDocTypes.OuterXml);
                XmlNodeList docTypeNodes = doc.SelectNodes("/response/DocumentTypes/DocumentType");

                string propertySetName = "";
                string propertySetID = "";
                foreach (XmlNode docTypeNode in docTypeNodes)
                {
                    string retrievedDocTypeId = getXMLAttributeValue(docTypeNode, "TypeID", "");
                    if (retrievedDocTypeId == request.documentTypeID.ToString())
                    {
                        //we found the document type
                        propertySetName = getXMLAttributeValue(docTypeNode, "PropertySetName", "");
                        propertySetID = getXMLAttributeValue(docTypeNode, "PropertySetID", "");
                        break; //get out of the loop quickly
                    }
                }

                //check if we have the property set
                if (string.IsNullOrEmpty(propertySetName))
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "No property set attached to Document Type with ID [" + request.documentTypeID + "]";
                    return resp;
                }

                //check if the property set we got from EDMS matches what's supplied in the request
                if(propertySetID != request.propertySetID.ToString())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "Property set ID supplied["+ request.propertySetID + "], doesnot match property set in EDMS["+ propertySetID + "] for the Document Type";
                    return resp;
                }
                                               
                //generate the property set
                string xmlPropertySet = "<Propertysets><propertyset Name=\"" + propertySetName + "\"><propertyrow ";
                foreach (var propSet in request.properties)
                {
                    string fieldName = propSet.fieldName;
                    string fieldValue = propSet.fieldValue;
                    string propRow = " " + fieldName + "=\"" + fieldValue + "\" ";
                    xmlPropertySet += propRow;
                }
                xmlPropertySet += " /></propertyset></Propertysets>";


                //attach the property set the document
                XmlNode xmlRespAttachPropSet = edmsService.AddPropertySetRow(authenticationTicket, targetEdmsPath, xmlPropertySet);
                if (xmlRespAttachPropSet.Attributes["success"].Value != "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = xmlRespAttachPropSet.Attributes["error"].Value;
                    return resp;
                }


                //successfully attached the property set, now we update the document type
                XmlNode xmlRespUpdateDocType = edmsService.UpdateDocumentType(authenticationTicket, targetEdmsPath, request.documentTypeID);
                if (xmlRespUpdateDocType.Attributes["success"].Value == "true")
                {
                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = "The Document has been successfully uploaded";
                    return resp;
                }
                else
                {
                    string error = xmlRespUpdateDocType.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        [HttpPost, Route("api/edmstools/get-edms-workflow-status")]
        public BaseResponseDocumentTasks fetchEdmsDocumentWorkFlowStatus(GetEdmsWorkflowStatusRequest request)
        {

            BaseResponseDocumentTasks resp = new BaseResponseDocumentTasks();

            try
            {

                edms.srv edmsService = new edms.srv();

                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }

                resp = getEDMSWorkflowStatus(edmsService, authenticationTicket, request.documentID);
                return resp;

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        [HttpGet, Route("api/edmstools/get-next-petty-cash-voucher-no/{regionalOffice}")]
        public BaseResponse getNextPettyCashVoucherNumber(string regionalOffice)
        {

            BaseResponse resp = new BaseResponse();
            try
            {

                if (string.IsNullOrEmpty(regionalOffice))
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "Please Supply the Regional Office Parameter";
                    return resp;
                }

                List<string> supportedRegionalOffices = new List<string> { Globals.REGIONAL_OFFICE_HEAD_OFFICE, Globals.REGIONAL_OFFICE_MBARARA, Globals.REGIONAL_OFFICE_MBALE, Globals.REGIONAL_OFFICE_GULU };

                if (!(supportedRegionalOffices.Contains(regionalOffice)))
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "Regional Office Not Suppported ["+regionalOffice+"]";
                    return resp;
                }
                
                DatabaseHandler dbHandler = new DatabaseHandler();
                var voucherNumber = dbHandler.generateNexPettyCashVoucherNumber(DatabaseHandler.dbConnection(), regionalOffice);

                if (string.IsNullOrEmpty(voucherNumber))
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "Failed to Generate Voucher Number";
                    return resp;
                }

                resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                resp.statusDescription = voucherNumber.ToString();
                resp.result = voucherNumber.ToString();
                return resp;
            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        [HttpGet, Route("api/edmstools/get-next-bt-number")]
        public BaseResponse getNextBtNumber()
        {

            BaseResponse resp = new BaseResponse();
            try
            {
                DatabaseHandler dbHandler = new DatabaseHandler();
                var voucherNumber = dbHandler.generateNextBtNumber(DatabaseHandler.dbConnection());

                resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                resp.statusDescription = voucherNumber.ToString();
                resp.result = voucherNumber.ToString();
                return resp;
            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }


        [HttpPost, Route("api/edmstools/get-edms-rejection-status")]
        public BaseResponseEdmsRejectionStatus getEdmsRejectionStatus(GetEdmsRejectionStatusRequest request)
        {

            BaseResponseEdmsRejectionStatus resp = new BaseResponseEdmsRejectionStatus();

            try
            {

                edms.srv edmsService = new edms.srv();

                if (request == null)
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "NULL REQUEST OBJECT SENT";
                    return resp;
                }

                if (!request.validRequest())
                {
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = request.errorDescription;
                    return resp;
                }

                string authenticationTicket;
                request.authenticationTicket = "";
                if (string.IsNullOrEmpty(request.authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                else
                {
                    authenticationTicket = request.authenticationTicket;
                }


                //list of rejected documents
                List<EdmsRejectedDocument> rejectedDocs = new List<EdmsRejectedDocument>();

                //list of completed documents
                List<EdmsDocumentWithCompletedWorkflow> completedDocs = new List<EdmsDocumentWithCompletedWorkflow>();

                foreach(int documentId in request.docIds)
                {

                    //get the tasks associated to this document ID
                    var respGetDocStatus = getEDMSWorkflowStatus(edmsService, authenticationTicket, documentId);

                    if(respGetDocStatus.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        //try another document
                        continue;
                    }
                    else
                    {

                        var tasks = respGetDocStatus.tasks;
                        if (tasks.Count <= 0)
                        {
                            continue;
                        }

                        //check if the workflow has been completed
                        var docWithCompletedWorkflow = checkForWorkflowCompletion(tasks, documentId.ToString());
                       
                        if(docWithCompletedWorkflow != null)
                        {
                            //workflow has been completed
                            completedDocs.Add(docWithCompletedWorkflow);
                            continue;
                        }

                        //workflow has not been completed so we proceed to see if there's a rejection anywhere
                        List<EdmsTask> sortedList = tasks.OrderByDescending(o => o.stepNumber).ToList();                      
                        EdmsRejectedDocument rejectedDoc = checkIfAnyOfTheTasksIsARejection(sortedList, documentId);
                        if (rejectedDoc != null)
                        {
                            rejectedDocs.Add(rejectedDoc);
                        }

                    }

                }

                //we successfully have a list of the rejected documents
                resp.rejectedDocuments = rejectedDocs;
                resp.documentsWithCompletedWorkflows = completedDocs;
                resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                resp.statusDescription = Globals.STATUS_DESC_SUCCESS;
                return resp;

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

        private EdmsRejectedDocument checkIfAnyOfTheTasksIsARejection(List<EdmsTask> tasks, int documentId)
        {
            
            foreach(var task in tasks)
            {

                if (task.isRejected())
                {
                    EdmsRejectedDocument doc = new EdmsRejectedDocument();
                    doc.edmsDocumentId = documentId.ToString();
                    doc.rejectUser = task.taskOwner;
                    doc.rejectDate = task.finishedOn;
                    return doc;
                }
            }

            return null;

        }

        [HttpGet, Route("api/edmstools/test-prop-set")]
        public BaseResponse GetDocumentPropertySetValueByPath()
        {

            BaseResponse resp = new BaseResponse();

            try
            {

                string documentPath = "/PPDA DEPARTMENTAL WORKSPACES/EMIS/test_222222222222222222_pk.pdf";
                string propertyFieldName = "SOURCE";
                string propertyFieldValue = "";
                
                string authenticationTicket = "";               
                if (string.IsNullOrEmpty(authenticationTicket))
                {

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.username = Globals.API_USERNAME;
                    loginRequest.password = Globals.API_PASSWORD;

                    BaseResponse loginResponse = Login(loginRequest);
                    if (loginResponse.statusCode != Globals.STATUS_CODE_SUCCESS)
                    {
                        resp.statusCode = loginResponse.statusCode;
                        resp.statusDescription = loginResponse.statusDescription;
                        return resp;
                    }

                    //we managed to get an authentication ticket
                    authenticationTicket = loginResponse.result;
                }
                

                XmlNode xmlResponse;
                edms.srv edmsService = new edms.srv();

                //something went wrong on the call to download
                bool withPropertySets = true;
                bool withSecurity = true;
                bool withOwner = true;
                bool withVersions = true;

                xmlResponse = edmsService.GetDocument(
                    authenticationTicket, documentPath, withPropertySets, withSecurity, withOwner, withVersions);

                if (xmlResponse.Attributes["success"].Value != "true")
                {
                    string error = xmlResponse.Attributes["error"].Value;
                    resp.statusCode = Globals.STATUS_CODE_FAILED;
                    resp.statusDescription = "EDMS ERROR - " + error;
                    return resp;
                }
                else
                {

                    var nodeDocument = xmlResponse.FirstChild;

                    if (nodeDocument != null)
                    {
                        string docId = getXMLAttributeValue(nodeDocument, "DocumentID", "");
                        string docName = getXMLAttributeValue(nodeDocument, "Name", "");
                        string docPath = getXMLAttributeValue(nodeDocument, "Path", "");

                        bool breakFromAllLoops = false;

                        foreach (XmlElement node in nodeDocument.ChildNodes)
                        {

                            if (node.Name == "Propertysets")
                            {
                                foreach(XmlElement propSetNode in node.ChildNodes)
                                {
                                    //loop through the prop set rows
                                    foreach(XmlElement propSetRow in propSetNode.ChildNodes)
                                    {
                                        propertyFieldValue = getXMLAttributeValue(propSetRow, propertyFieldName, "");
                                        breakFromAllLoops = true;
                                        break;
                                    }

                                    if (breakFromAllLoops)
                                    {
                                        break;
                                    }

                                }

                                if (breakFromAllLoops)
                                {
                                    break;
                                }

                            }

                        }

                    }

                    if (string.IsNullOrEmpty(propertyFieldValue))
                    {
                        resp.statusCode = Globals.STATUS_CODE_FAILED;
                        resp.statusDescription = "PROPERTY FIELD VALUE NOT FOUND";
                        return resp;
                    }


                    resp.statusCode = Globals.STATUS_CODE_SUCCESS;
                    resp.statusDescription = Globals.STATUS_DESC_SUCCESS;
                    resp.result = propertyFieldValue;
                    return resp;

                }

            }
            catch (Exception ex)
            {
                resp.statusCode = Globals.STATUS_CODE_FAILED;
                resp.statusDescription = Globals.generalError(ex.Message);
                return resp;
            }

        }

    }

}
