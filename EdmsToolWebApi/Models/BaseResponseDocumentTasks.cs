﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class BaseResponseDocumentTasks : BaseResponse
    {
        public List<EdmsTask> tasks = new List<EdmsTask>();
    }
}