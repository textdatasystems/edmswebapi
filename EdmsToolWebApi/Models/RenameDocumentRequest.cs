﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class RenameDocumentRequest
    {
        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string currentDocumentPath = "";
        [DataMember]
        public string newDocumentName = "";
        [DataMember]
        public string newDescription = "";
        [IgnoreDataMember]
        public string errorDescription = "";

        internal bool validRequest()
        {

            if (string.IsNullOrEmpty(currentDocumentPath))
            {
                errorDescription = "PLEASE SUPPLY A VALID EDMS DOCUMENT PATH";
                return false;
            }

            if (string.IsNullOrEmpty(newDocumentName))
            {
                errorDescription = "PLEASE SUPPLY A VALID DOCUMENT NAME";
                return false;
            }

            return true;

        }
    }

}