﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class BtFormDataGroup
    {

        public string activityTitle = "";
        public string subTotal = "";
        public List<BtFormItem> items = new List<BtFormItem>();

    }

}