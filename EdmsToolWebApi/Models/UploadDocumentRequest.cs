﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class UploadDocumentRequest
    {
        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string targetEdmsPath = "";
        [DataMember]
        public string documentContents = "";
        [IgnoreDataMember]
        public string errorDescription = "";

        internal bool validRequest()
        {

            if (string.IsNullOrEmpty(targetEdmsPath))
            {
                errorDescription = "PLEASE SUPPLY A VALID TARGET EDMS PATH";
                return false;
            }

            if (string.IsNullOrEmpty(documentContents))
            {
                errorDescription = "PLEASE SUPPLY A VALID BASE 64 DOCUMENT CONTENT";
                return false;
            }

            return true;

        }
    }
}