﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class DownloadDocumentRequest
    {
        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string edmsDocumentPath = "";
        [IgnoreDataMember]
        public string errorDescription = "";

        internal bool validRequest()
        {

            if (string.IsNullOrEmpty(edmsDocumentPath))
            {
                errorDescription = "PLEASE SUPPLY A VALID EDMS PATH";
                return false;
            }

            return true;
        }
    }
}