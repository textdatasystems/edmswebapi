﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class BaseResponse
    {
        [DataMember]
        public string statusCode = "";
        [DataMember]
        public string statusDescription = "";
        [DataMember]
        public string result = "";
        [DataMember]
        public string createdEDMSDocumentId = "";
    }
}