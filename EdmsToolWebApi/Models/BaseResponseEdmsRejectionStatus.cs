﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class BaseResponseEdmsRejectionStatus : BaseResponse
    {
        public List<EdmsRejectedDocument> rejectedDocuments = new List<EdmsRejectedDocument>();
        public List<EdmsDocumentWithCompletedWorkflow> documentsWithCompletedWorkflows = new List<EdmsDocumentWithCompletedWorkflow>();
    }

}