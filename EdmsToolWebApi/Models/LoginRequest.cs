﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class LoginRequest
    {
        [DataMember]
        public string username = "";
        [DataMember]
        public string password = "";
        [IgnoreDataMember]
        public string errorDescription = "";

        internal bool validRequest()
        {

            if (string.IsNullOrEmpty(username))
            {
                errorDescription = "PLEASE SUPPLY A VALID USERNAME";
                return false;
            }

            if (string.IsNullOrEmpty(password))
            {
                errorDescription = "PLEASE SUPPLY A VALID PASSWORD";
                return false;
            }

            return true;

        }

    }
}