﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class EdmsDocumentWithCompletedWorkflow
    {
        public string edmsDocumentId = "";
        public string completedBy = "";
        public string completionDate = "";
        public string approvalStatus = "";
    }
}