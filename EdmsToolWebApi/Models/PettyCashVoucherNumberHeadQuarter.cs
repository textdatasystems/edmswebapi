﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class PettyCashVoucherNumberHeadQuarter
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string VoucherNumber { get; set; }
    }
}