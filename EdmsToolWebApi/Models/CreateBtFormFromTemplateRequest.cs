﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class CreateBtFormFromTemplateRequest
    {

        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string newDocumentPath = "";
        [IgnoreDataMember]
        public string errorDescription = "";

        //new data fields

        //header fields
        public string dtSourceOfFunding = "";
        public string dtEmisRequisitionId = "";

        //to head of finance
        public string dtHeadFinanceDate = "";
        public string dtHeadFinanceClaimant = "";
        public string dtHeadFinanceInitials = "";
        public string dtHeadFinanceDesignation = "";
        public string dtHeadFinanceDepartment = "";
        public string dtHeadFinanceClaimSubject = "";

        //budget lines
        public List<BtFormDataGroup> dataGroups = new List<BtFormDataGroup>();

        //bt total
        public string dtTotal = "";
        public string dtAmountWords = "";

        //claimant
        public string dtClaimantName = "";
        public string dtClaimantInitials = "";
        public string dtClaimantDepartment = "";
        public string dtClaimantDate = "";
        public string dtClaimantComments = "";

        //hod
        public string dtHodName = "";
        public string dtHodDepartment = "";        
        public string dtHodInitials = "";
        public string dtHodDate = "";
        public string dtHodDesignation = ""; //nolonger used
        public string dtHodComments = "";



        /*
         * 
         * old data fields
         *
         */

        public List<BtFormItem> items = new List<BtFormItem>();

        //bt form data variables
        public string dtActivity1Desc = "";
        public string dtActivity1EstimatedCost = "";
        public string dtActivity2Desc = "";
        public string dtActivity2EstimatedCost = "";
        public string dtTotalEstimatedCost = "";

        public string dtRequestorName = "";
        public string dtRequestorDepartment = "";
        public string dtRequestorInitials = "";
        public string dtRequestorDate = "";
  
        internal bool validRequest()
        {

            if (string.IsNullOrEmpty(newDocumentPath))
            {
                errorDescription = "PLEASE SUPPLY A VALID EDMS DOCUMENT PATH IN THE FIELD [newDocumentPath]";
                return false;
            }
            return true;

        }

    }

}