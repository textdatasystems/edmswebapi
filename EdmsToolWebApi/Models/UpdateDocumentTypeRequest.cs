﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class UpdateDocumentTypeRequest
    {
        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string edmsDocumentPath = "";
        [DataMember]
        public string edmsDocumentTypeId = "";
        [IgnoreDataMember]
        public string errorDescription = "";

        internal int documentTypeId;

        internal bool validRequest()
        {

            if (string.IsNullOrEmpty(edmsDocumentPath))
            {
                errorDescription = "PLEASE SUPPLY A VALID EDMS DOCUMENT PATH";
                return false;
            }

            if (string.IsNullOrEmpty(edmsDocumentTypeId))
            {
                errorDescription = "PLEASE SUPPLY A VALID EDMS DOCUMENT TYPE ID";
                return false;
            }

            if(!int.TryParse(edmsDocumentTypeId, out documentTypeId))
            {
                errorDescription = "EDMS DOCUMENT TYPE ID MUST BE AN INTEGER";
                return false;
            }

            return true;

        }

    }

}