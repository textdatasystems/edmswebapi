﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class CreateBtFormAuditMergedFromTemplateRequest
    {

        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string newDocumentPath = "";
        [IgnoreDataMember]
        public string errorDescription = "";
        
        //bt form data variables

        //header
        public string dtSenderName = "";
        public string dtSenderDesign = "";
        public string dtSenderDept = "";
        public string dtClaimDate = "";
        public string dtClaimFor = "";

        //claimant
        public string dtClaimantName = "";
        public string dtClaimantDept = "";
        public string dtClaimantDate = "";
        public string dtClaimantInitial = "";
        public string dtClaimantRecom = "";
        public string dtClaimantComments = "";

        //totals
        public string dtTotalAllowances = "";
        public string dtTotalAmountFuel = "";
        public string dtTotalAmountClaim = "";
        public string dtAmountWords = "";

        //activities
        public ActivityDetail activity1 = null;
        public ActivityDetail activity2 = null;
        public ActivityDetail activity3 = null;
        public ActivityDetail activity4 = null;
        public ActivityDetail activity5 = null;
        public ActivityDetail activity6 = null;
        public ActivityDetail activity7 = null;
        public ActivityDetail activity8 = null;

        //fuel lines
        public FuelLine fuelLine1 = null;
        public FuelLine fuelLine2 = null;
        public FuelLine fuelLine3 = null;
        public FuelLine fuelLine4 = null;


        internal bool validRequest()
        {

            if (string.IsNullOrEmpty(newDocumentPath))
            {
                errorDescription = "PLEASE SUPPLY A VALID EDMS DOCUMENT PATH IN THE FIELD [newDocumentPath]";
                return false;
            }
            return true;

        }
        
    }

}