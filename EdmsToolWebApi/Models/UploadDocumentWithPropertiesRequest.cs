﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class UploadDocumentWithPropertiesRequest
    {

        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string targetEdmsPath = "";
        [DataMember]
        public string documentContents = "";
        public string edmsPropertySetID = "";
        public string edmsDocumentTypeID = "";
        public List<EdmsPropertySetRow> properties = new List<EdmsPropertySetRow>();

        internal int documentTypeID;        
        internal int propertySetID;        

        [IgnoreDataMember]
        public string errorDescription = "";

        internal bool validRequest()
        {

            if (string.IsNullOrEmpty(targetEdmsPath))
            {
                errorDescription = "PLEASE SUPPLY A VALID TARGET EDMS PATH";
                return false;
            }

            if (string.IsNullOrEmpty(documentContents))
            {
                errorDescription = "PLEASE SUPPLY A VALID BASE 64 DOCUMENT CONTENT";
                return false;
            }

            if(!int.TryParse(edmsDocumentTypeID, out documentTypeID))
            {
                errorDescription = "INVALID DOCUMENT TYPE ID SUPPLIED[" + edmsDocumentTypeID + "]";
                return false;
            }

            if (!int.TryParse(edmsPropertySetID, out propertySetID))
            {
                errorDescription = "INVALID PROPERTY SET ID SUPPLIED[" + propertySetID + "]";
                return false;
            }

            return true;

        }

    }

}