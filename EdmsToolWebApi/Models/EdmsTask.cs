﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class EdmsTask
    {

        public int stepNumber = 0;
        public string stepName = "";
        public string taskName = "";
        public string taskOwner = "";
        public string taskStatus = "";
        public string startDate = "";    
        public string dueDate = "";
        public string finishedOn = "";
        public string assignedVersion = "";
        public string endVersion = "";
        public string assignedBy = "";
        public string approvalStatus = "";

        internal bool isRejected()
        {
            return this.approvalStatus == "Rejected";
        }

    }

}