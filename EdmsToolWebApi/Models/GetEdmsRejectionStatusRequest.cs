﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class GetEdmsRejectionStatusRequest
    {

        [DataMember]
        public string authenticationTicket = "";
        public List<string> edmsDocumentIds = new List<string>();

        internal List<int> docIds = new List<int>();

        [IgnoreDataMember]
        public string errorDescription = "";

        internal bool validRequest()
        {

            if (edmsDocumentIds.Count == 0)
            {
                errorDescription = "PLEASE SUPPLY DOCUMENT IDS IN THE [" + edmsDocumentIds + "] FIELD";
                return false;
            }

            //check that all the document ids are integers
            foreach (string docId in edmsDocumentIds)
            {
                int documentID = 0;
                if (!int.TryParse(docId, out documentID))
                {
                    errorDescription = "INVALID DOCUMENT ID SUPPLIED[" + docId + "] IN DOCUMENT ID LIST, VALUES MUST BE INTERGERS";
                    return false;
                }
                else
                {
                    docIds.Add(documentID);
                }

            }

            return true;

        }


    }

}