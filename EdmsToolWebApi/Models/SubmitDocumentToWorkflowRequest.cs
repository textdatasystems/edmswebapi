﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class SubmitDocumentToWorkflowRequest
    {
        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string edmsFolderPath = "";
        [DataMember]
        public string edmsFlowDefId = "";
        [IgnoreDataMember]
        public string errorDescription = "";

        internal bool validRequest()
        {

            //if (string.IsNullOrEmpty(edmsFolderPath))
            //{
            //    errorDescription = "PLEASE SUPPLY A VALID EDMS FOLDER PATH";
            //    return false;
            //}

            //if (string.IsNullOrEmpty(edmsFlowDefId))
            //{
            //    errorDescription = "PLEASE SUPPLY A VALID EDMS FLOW DEF ID";
            //    return false;
            //}

            return true;

        }

    }

}