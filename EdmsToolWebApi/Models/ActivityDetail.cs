﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class ActivityDetail
    {
        public string startDate = "";
        public string endDate = "";
        public string teamOrPde = "";
        public string noOfDays = "";

        public List<StaffPerdiem> staffPerdiem = new List<StaffPerdiem>();
        public string staffPerdiemTotal = "";

    }

}