﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class CreateFormFiveFromTemplateRequest
    {

        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string newDocumentPath = "";
        [IgnoreDataMember]
        public string errorDescription = "";

    
        //form 5 data variables
        public string dtProcEntityCode = "";
        public string dtEmisRequisitionId = "";
        public string dtProcType = "";
        public string dtFinancialYear = "";
        public string dtSequenceNumber = "";
        public string dtSubject = "";
        public string dtProcPlanRef = "";
        public string dtDeliveryLocation = "";
        public string dtDateRequired = "";

        public string dtRequestorName = "";
        public string dtRequestorInitials = "";
        public string dtRequestorTitle = "";
        public string dtRequestorDate = "";

        public string dtHodConfirmationName = "";
        public string dtHodConfirmationInitials = "";
        public string dtHodConfirmationTitle = "";
        public string dtHodConfirmationDate = "";

        public string dtCurrency = "";
        public string dtEstimatedTotalCost = "";
        public string dtAmountWords = "";

        public List<FormFiveItem> items = new List<FormFiveItem>();
               

        internal bool validRequest()
        {
            
            if (string.IsNullOrEmpty(newDocumentPath))
            {
                errorDescription = "PLEASE SUPPLY A VALID EDMS DOCUMENT PATH IN THE FIELD [newDocumentPath]";
                return false;
            }

            return true;

        }
    }
}