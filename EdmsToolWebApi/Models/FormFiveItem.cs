﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class FormFiveItem
    {

        public string itemNo = "";
        public string description = "";
        public string quantity = "";
        public string unitOfMeasure = "";
        public string estimatedUnitCost = "";
        public string marketPriceOfProcurement = "";

    }

}