﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class DownloadDocumentByIdRequest
    {

        [DataMember]
        public string authenticationTicket = "";
        [DataMember]
        public string documentId = "";
        [IgnoreDataMember]
        public string errorDescription = "";
        internal bool validRequest()
        {
            int docId = 0;
            if (string.IsNullOrEmpty(documentId) || !int.TryParse(documentId, out docId))
            {
                errorDescription = "PLEASE SUPPLY A VALID DOCUMENT ID";
                return false;
            }
            return true;
        }
    }

}