﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class EdmsRejectedDocument
    {
        public string edmsDocumentId = "";
        public string rejectUser = "";
        public string rejectDate = "";
    }
}