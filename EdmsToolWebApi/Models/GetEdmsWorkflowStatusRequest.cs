﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EdmsToolWebApi.Models
{
    public class GetEdmsWorkflowStatusRequest
    {

        [DataMember]
        public string authenticationTicket = "";
        public string edmsDocumentID = "";

        internal int documentID;

        [IgnoreDataMember]
        public string errorDescription = "";

        internal bool validRequest()
        {

            if (!int.TryParse(edmsDocumentID, out documentID))
            {
                errorDescription = "INVALID DOCUMENT ID SUPPLIED[" + edmsDocumentID + "], VALUE MUST BE AN INTERGER";
                return false;
            }
            
            return true;

        }

    }
}